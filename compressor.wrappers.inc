<?php

class DrupalThemeStreamWrapper extends DrupalLocalStreamWrapper {
  public function getDirectoryPath() {
    static $path;
    if ( !empty($path) ){
      return $path;
    }

    global $theme_key;
    $path = drupal_get_path('theme', $theme_key);

    return $path;
  }

  function getExternalUrl() {
    $path = str_replace('\\', '/', $this->getTarget());
    return $GLOBALS['base_url'] . '/' . self::getDirectoryPath() . '/' . drupal_encode_path($path);
  }
}
